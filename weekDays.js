var inputArray=[['Issue1','W1'],['Issue1','W1'],['Issue2','W1'],['Issue3','W1'],
                ['Issue1','W2'],['Issue2','W3'],['Issue3','W4'],['Issue1','W2'],
                ['Issue2','W3'],['Issue3','W4'],['Issue3','W5'],['Issue7','W5'],
                ['Issue1','W1'],['Issue1','W1'],['Issue2','W1'],['Issue3','W1'],
                ['Issue1','W2'],['Issue2','W3'],['Issue3','W4'],['Issue1','W2'],
                ['Issue2','W3'],['Issue3','W4'],['Issue3','W5'],['Issue7','W5'],
               ['Issue1','W1'],['Issue1','W1'],['Issue2','W1'],['Issue3','W1'],
                ['Issue1','W2'],['Issue2','W3'],['Issue3','W4']
               
               ]
var currDate=new Date();

function Main(){
  Logger.log("Week Day Is: "+currDate.getWeekOfMonth());
  var returnArr=[]
  var objArr=[W1={"time":'W1'},W2={"time":'W2'},W3={"time":'W3'},W4={"time":'W4'},W5={"time":'W5'},W6={"time":'W6'}];
  for(var i=0;i<inputArray.length;i++){
      var value=inputArray[i];
        if(value[1]=='W1'){
          objArr[0][value[0]] = 1 + (objArr[0][value[0]] || 0);
        }else if(value[1]=='W2'){
        objArr[1][value[0]] = 1 + (objArr[1][value[0]] || 0);
        }else if(value[1]=='W3'){
        objArr[2][value[0]] = 1 + (objArr[2][value[0]] || 0);
        }else if(value[1]=='W4'){
        objArr[3][value[0]] = 1 + (objArr[3][value[0]] || 0);
        }else if(value[1]=='W5'){
        objArr[4][value[0]] = 1 + (objArr[4][value[0]] || 0);
        }else if(value[1]=='W6'){
        objArr[5][value[0]] = 1 + (objArr[5][value[0]] || 0);
        }
      }
  for(var j=0;j<objArr.length;j++){
  if(Object.keys(objArr[j]).length>1){
  returnArr.push(objArr[j])
  }
}
  Logger.log("Data Source Obj is:"+JSON.stringify(returnArr));
  return returnArr;
}


var dataSource = [{
    time: "",
    issue1: 6.7,
    issue2: 28.6,
    issue3: 5.1
},{
    state: "Germany",
    young: 6.7,
    middle: 28.6,
    older: 5.1
}];
//Thanks: https://stackoverflow.com/questions/3280323/get-week-of-the-month
Date.prototype.getWeekOfMonth = function(exact) {
        var month = this.getMonth()
            , year = this.getFullYear()
            , firstWeekday = new Date(year, month, 1).getDay()
            , lastDateOfMonth = new Date(year, month + 1, 0).getDate()
            , offsetDate = this.getDate() + firstWeekday - 1
            , index = 1 // start index at 0 or 1, your choice
            , weeksInMonth = index + Math.ceil((lastDateOfMonth + firstWeekday - 7) / 7)
            , week = index + Math.floor(offsetDate / 7)
        ;
        if (exact || week < 2 + index) return week;
        return week === weeksInMonth ? index + 5 : week;
    };
    
    // Simple helper to parse YYYY-MM-DD as local
    function parseISOAsLocal(s){
      var b = s.split(/\D/);
      return new Date(b[0],b[1]-1,b[2]);
 }